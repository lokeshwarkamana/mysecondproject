package com.example.Bank2;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Bank2Controller {
	
@RequestMapping("/branches")
public String Branches() {
	return "branches";
}

@RequestMapping("/services")
public String Services() {
	return "services";
}

}
